# Mini Project 2

This project intends to create a website using Streamlit, connect to an open source LLM (Hugging Face), and deploy model via Streamlit  that can be accessed by anyone via browser.

## Components

- **Demo Vedio**: Showing my app running
[Watch the video](https://youtu.be/JCSNiw2Q48M?si=cYEIOG1I2KwAnWgK)

- **Test on running locally**: Before deployment, test my app locally
  ![Test on running locally](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG133.jpg)


- **Screenshort for my app**: These screenshorts show how my app works
  ![Initial Page](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG130.jpg)
  ![Second Page](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG131.jpg)

- **Deployment on Streamlit**: This shows the successful deployment on Streamlit
  ![Deployment on Streamlit](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG134.jpg)


## Steps

1. Create a Github repository
2. Set up Python development environment
3. Run `pip install streamlit` to install Streamlit
4. Create a requirements.txt file in the Python project
5. Add the following requirements:
```txt
streamlit 
transformers
tensorflow
tf-keras
```
6. Run `pip install -r requirements.txt` to install the packages
7. Implement the `streamlitapp.py` to design my app
8. Run `streamlit run streamlitapp.py` to test locally
9. Create a Streamlit account and sign in
10. Deploy my application to Streamlit