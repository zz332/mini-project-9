import streamlit as st
from transformers import pipeline

model = pipeline("text-generation", model="openai-gpt")

def main():
    st.set_page_config(
        page_title="AI Text Generator",
        page_icon=":zap:",
        layout="wide",
        initial_sidebar_state="collapsed"
    )
    
    header_html = """
    <div style="background-color: #4e73df; padding: 20px; border-radius: 10px;">
        <h1 style="color: white; text-align: center;">⚡ AI Text Generator with Hugging Face</h1>
    </div>
    <br />
    """
    st.markdown(header_html, unsafe_allow_html=True)
    
    st.markdown("""
    <div style="background-color: #4e73df; padding: 20px; border-radius: 10px;">
        <p style="color: white;">Discover the power of AI text generation with our Streamlit app, leveraging the Hugging Face Transformers library. Enter a prompt and let the AI amaze you with its creative responses.</p>
    </div>
    """, unsafe_allow_html=True)
    
    text_input = st.text_area("Type here to start generating text...", height=200)
    
    st.markdown("<style> .stButton button {background-color: #4e73df; color: white;}</style>", unsafe_allow_html=True)
    if st.button("Generate Text", key="generate_button", help="Click to generate text"):
        if text_input:
            generated_text = model(text_input, max_length=50, do_sample=True)[0]['generated_text']
            
            st.markdown("## Generated Text:")
            st.markdown(f"<div style='background-color: #4e73df; padding: 20px; border-radius: 10px; color: white;'>{generated_text}</div>", unsafe_allow_html=True)
        else:
            st.warning("Please enter some text to start generating.")

if __name__ == "__main__":
    main()

